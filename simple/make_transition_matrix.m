pit_growth_rate = 0.1524; % in mm/year

thickness = 5.08; % in mm (approx 0.2 inches)
N = 200;          % divisions in thickness

dt = 0.1; % Markov chain evaluated every dt year

p = (pit_growth_rate*dt)/(thickness/N); % Probability of pit growth

M = eye(N)*(1-p);   % Diagonal elements are set to (1-p), i.e. the probability of not growing 

for i = 1:N-1
    M(i,i+1) = p ;
end

M(N,N) = 1.0 ;  % A completely pitted surface remains pitted

i = zeros(N,1); i(1,1) = 1;    % Initial probability matrix

length_mat = linspace(0,thickness,N);
