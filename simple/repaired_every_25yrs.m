make_transition_matrix


% After 20 years
n = round(20/dt);
i_20_yrs = (M^n)'*i;


% Do_correction_after_25_yrs
n = round(25/dt);
i_after_25_yrs = (M^n)'*i;

sum_add = 0;
for index = size(i_after_25_yrs)*0.80:size(i_after_25_yrs)-1
    sum_add = sum_add + i_after_25_yrs(index);
    i_after_25_yrs(index) = 0;
end
i_after_25_yrs(1) = i_after_25_yrs(1)+sum_add;


% After 40 years
n = round((40-25)/dt);
i_40_yrs_correct = (M^n)'*i_after_25_yrs;



% Do_correction_after_25_yrs
n = round((50-25)/dt);
i_after_50_yrs = (M^n)'*i_after_25_yrs;

sum_add = 0;
for index = size(i_after_50_yrs)*0.80:size(i_after_50_yrs)-1
    sum_add = sum_add + i_after_50_yrs(index);
    i_after_50_yrs(index) = 0;
end
i_after_50_yrs(1) = i_after_50_yrs(1)+sum_add;


% After 60 years
n = round((60-50)/dt);
i_60_yrs_correct = (M^n)'*i_after_50_yrs;



plot(length_mat,i_20_yrs,'b-','Linewidth', 1.5)
hold on;
plot(length_mat,i_40_yrs_correct,'r-','Linewidth', 1.5)
plot(length_mat,i_60_yrs_correct,'k-','Linewidth', 1.5)

xlabel('Pit depth (mm)')
ylabel('Probability density')
legend('20 years','40 years','60 years')
