make_transition_matrix

% Do_correction_after_15_yrs
n = round(15/dt);
i_after_15_yrs = (M^n)'*i;

sum_add = 0;
for index = size(i_after_15_yrs)*0.80:size(i_after_15_yrs)-1
    sum_add = sum_add + i_after_15_yrs(index);
    i_after_15_yrs(index) = 0;
end
i_after_15_yrs(1) = i_after_15_yrs(1)+sum_add;

% After 20 years
n = round((20-15)/dt);
i_20_yrs_correct = (M^n)'*i_after_15_yrs;





% Do_correction_after_30_yrs
n = round((30-15)/dt);
i_after_30_yrs = (M^n)'*i_after_15_yrs;

sum_add = 0;
for index = size(i_after_30_yrs)*0.80:size(i_after_30_yrs)-1
    sum_add = sum_add + i_after_30_yrs(index);
    i_after_30_yrs(index) = 0;
end
i_after_30_yrs(1) = i_after_30_yrs(1)+sum_add;

% After 40 years
n = round((40-30)/dt);
i_40_yrs_correct = (M^n)'*i_after_30_yrs;



% Do_correction_after_30_yrs
n = round((45-30)/dt);
i_after_45_yrs = (M^n)'*i_after_30_yrs;

sum_add = 0;
for index = size(i_after_45_yrs)*0.80:size(i_after_45_yrs)-1
    sum_add = sum_add + i_after_45_yrs(index);
    i_after_45_yrs(index) = 0;
end
i_after_45_yrs(1) = i_after_45_yrs(1)+sum_add;


% After 60 years
n = round((60-45)/dt);
i_60_yrs_correct = (M^n)'*i_after_45_yrs;
    



plot(length_mat,i_20_yrs_correct,'b-','Linewidth', 1.5)
hold on;
plot(length_mat,i_40_yrs_correct,'r-','Linewidth', 1.5)
plot(length_mat,i_60_yrs_correct,'k-','Linewidth', 1.5)

xlabel('Pit depth (mm)')
ylabel('Probability density')
legend('20 years','40 years','60 years')
