make_transition_matrix

% Do_correction_after_5_yrs
n = round(5/dt);
i_after_5_yrs = (M^n)'*i;

sum_add = 0;
for index = size(i_after_5_yrs)*0.80:size(i_after_5_yrs)-1
    sum_add = sum_add + i_after_5_yrs(index);
    i_after_5_yrs(index) = 0;
end
i_after_5_yrs(1) = i_after_5_yrs(1)+sum_add;


% Do_correction_after_10_yrs
n = round((10-5)/dt);
i_after_10_yrs = (M^n)'*i_after_5_yrs;

sum_add = 0;
for index = size(i_after_10_yrs)*0.80:size(i_after_10_yrs)-1
    sum_add = sum_add + i_after_10_yrs(index);
    i_after_10_yrs(index) = 0;
end
i_after_10_yrs(1) = i_after_10_yrs(1)+sum_add;


% Do_correction_after_15_yrs
n = round((15-10)/dt);
i_after_15_yrs = (M^n)'*i_after_10_yrs;

sum_add = 0;
for index = size(i_after_15_yrs)*0.80:size(i_after_15_yrs)-1
    sum_add = sum_add + i_after_15_yrs(index);
    i_after_15_yrs(index) = 0;
end
i_after_15_yrs(1) = i_after_15_yrs(1)+sum_add;



% After 20 years
n = round((20-15)/dt);
i_20_yrs_correct = (M^n)'*i_after_15_yrs;








% Do_correction_after_20_yrs
n = round((20-15)/dt);
i_after_20_yrs = (M^n)'*i_after_15_yrs;

sum_add = 0;
for index = size(i_after_20_yrs)*0.80:size(i_after_20_yrs)-1
    sum_add = sum_add + i_after_20_yrs(index);
    i_after_20_yrs(index) = 0;
end
i_after_20_yrs(1) = i_after_20_yrs(1)+sum_add;




% Do_correction_after_25_yrs
n = round((25-20)/dt);
i_after_25_yrs = (M^n)'*i_after_20_yrs;

sum_add = 0;
for index = size(i_after_25_yrs)*0.80:size(i_after_25_yrs)-1
    sum_add = sum_add + i_after_25_yrs(index);
    i_after_25_yrs(index) = 0;
end
i_after_25_yrs(1) = i_after_25_yrs(1)+sum_add;



% Do_correction_after_30_yrs
n = round((30-25)/dt);
i_after_30_yrs = (M^n)'*i_after_25_yrs;

sum_add = 0;
for index = size(i_after_30_yrs)*0.80:size(i_after_30_yrs)-1
    sum_add = sum_add + i_after_30_yrs(index);
    i_after_30_yrs(index) = 0;
end
i_after_30_yrs(1) = i_after_30_yrs(1)+sum_add;




% Do_correction_after_35_yrs
n = round((35-30)/dt);
i_after_35_yrs = (M^n)'*i_after_30_yrs;

sum_add = 0;
for index = size(i_after_35_yrs)*0.80:size(i_after_35_yrs)-1
    sum_add = sum_add + i_after_35_yrs(index);
    i_after_35_yrs(index) = 0;
end
i_after_35_yrs(1) = i_after_35_yrs(1)+sum_add;



% After 40 years
n = round((40-35)/dt);
i_40_yrs_correct = (M^n)'*i_after_35_yrs;




% Do_correction_after_40_yrs
n = round((40-35)/dt);
i_after_40_yrs = (M^n)'*i_after_35_yrs;

sum_add = 0;
for index = size(i_after_40_yrs)*0.80:size(i_after_40_yrs)-1
    sum_add = sum_add + i_after_40_yrs(index);
    i_after_40_yrs(index) = 0;
end
i_after_40_yrs(1) = i_after_40_yrs(1)+sum_add;



% Do_correction_after_45_yrs
n = round((45-40)/dt);
i_after_45_yrs = (M^n)'*i_after_40_yrs;

sum_add = 0;
for index = size(i_after_45_yrs)*0.80:size(i_after_45_yrs)-1
    sum_add = sum_add + i_after_45_yrs(index);
    i_after_45_yrs(index) = 0;
end
i_after_45_yrs(1) = i_after_45_yrs(1)+sum_add;



% Do_correction_after_50_yrs
n = round((50-45)/dt);
i_after_50_yrs = (M^n)'*i_after_45_yrs;

sum_add = 0;
for index = size(i_after_50_yrs)*0.80:size(i_after_50_yrs)-1
    sum_add = sum_add + i_after_50_yrs(index);
    i_after_50_yrs(index) = 0;
end
i_after_50_yrs(1) = i_after_50_yrs(1)+sum_add;



% Do_correction_after_55_yrs
n = round((55-50)/dt);
i_after_55_yrs = (M^n)'*i_after_50_yrs;

sum_add = 0;
for index = size(i_after_55_yrs)*0.80:size(i_after_55_yrs)-1
    sum_add = sum_add + i_after_55_yrs(index);
    i_after_55_yrs(index) = 0;
end
i_after_55_yrs(1) = i_after_55_yrs(1)+sum_add;



% After 60 years
n = round((60-55)/dt);
i_60_yrs_correct = (M^n)'*i_after_55_yrs;




plot(length_mat,i_20_yrs_correct,'b-','Linewidth', 1.5)
hold on;
plot(length_mat,i_40_yrs_correct,'r-','Linewidth', 1.5)
plot(length_mat,i_60_yrs_correct,'k-','Linewidth', 1.5)

xlabel('Pit depth (mm)')
ylabel('Probability density')
legend('20 years','40 years','60 years')
