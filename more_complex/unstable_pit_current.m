No_real = 200; % Number of realizations to average over

lambda = 0.005;
mu = 0.1;

state = zeros(No_real,1);

dt = 0.1 ;

N = 20000000;          % divisions in thickness

current = zeros(floor(200/dt),1);


for iteration = 1:floor(200/dt)
    
    my_random = rand(size(state));
    
    for index = 1:No_real
        
        if state(index) == 0
            if my_random(index) < lambda
                state(index) = 1;
                current(iteration) = current(iteration)+state(index);
            end
        elseif state(index) < 100
            if my_random(index) < mu
                state(index) = 0 ;
            else
                state(index) = state(index) + 1 ;
                current(iteration) = current(iteration)+state(index);
            end
        elseif state(index) >= N
            state(index) = N;
        else
            state(index) = state(index) + 1 ;
            current(iteration) = current(iteration)+state(index);
        end
        
    end
    
end






plot(log(1:iteration),log(current),'k-','Linewidth', 1.5)
xlabel('Log(Time)')
ylabel('Log(current)')
